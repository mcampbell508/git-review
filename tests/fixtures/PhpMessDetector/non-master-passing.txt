
Filtering changed files on branch using the following paths:
============================================================

 * app/
 * fruits/*/tests

Modified files on branch "pineapples"

app/example1.php - added
fruits/pineapples/tests/example2.php - modified
fruits/oranges/tests/example3.php - added

Running command:

php vendor/bin/phpmd app/example1.php,fruits/pineapples/tests/example2.php,fruits/oranges/tests/example3.php text phpmd.xml


 [OK] PHP Mess Detector checks passed, good job!!!!                             

