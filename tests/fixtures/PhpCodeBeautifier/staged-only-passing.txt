
Filtering changed files on branch using the following paths:
============================================================

 * app/
 * fruits/*/tests

Modified files on branch "pineapples"

app/example1.php - added
fruits/oranges/tests/example3.php - added
fruits/pineapples/tests/example2.php - modified

Running command:

php vendor/bin/phpcbf app/example1.php fruits/oranges/tests/example3.php fruits/pineapples/tests/example2.php -p


 [OK] PHP Code Beautifier had nothing to fix, good job!!!!                      

