<?php

namespace Shopworks\Tests\Integration;

use Shopworks\Git\Review\Process\Process;
use Shopworks\Tests\TestCase;

class GitReviewExecutableTest extends TestCase
{
    /** @test */
    public function it_displays_the_command_options_correctly(): void
    {
        $process = new Process("./git-review");
        $process->run();

        $this->assertTrue($process->isSuccessful());

        $processOutput = \trim($process->getOutput());

        $this->assertContains(
            "  USAGE: git-review <command> [options] [arguments]",
            $processOutput
        );

        $this->assertContains(
            "  es-lint       Run ESLint on only the changed files on a Git topic branch. All files on "
            . "`master` will be checked.",
            $processOutput
        );

        $this->assertContains(
            "  php-cs-fixer  Run PHP-CS-Fixer on only the changed files on a Git topic branch. All files on "
            . "`master` will be checked.",
            $processOutput
        );

        $this->assertContains(
            "  phpcs         Run PHP Code Sniffer on only the changed files on a Git topic branch. All files on "
            . "`master` will be checked.",
            $processOutput
        );

        $this->assertContains(
            "  phpcbf        Run PHP Code Beautifier on only the changed files on a Git topic branch. All files on "
            . "`master` will be checked.",
            $processOutput
        );

        $this->assertContains(
            "  phpmd         Run PHP Mess Detector on only the changed files on a Git topic branch. All files on "
            . "`master` will be checked.",
            $processOutput
        );

        $this->assertContains(
            "  parallel-lint Run PHP Parallel Lint on only the changed files on a Git topic branch. All files on "
            . "`master` will be checked.",
            $processOutput
        );

        $this->assertContains(
            "  phpstan       Run PHPStan on only the changed files on a Git topic branch. All files on "
            . "`master` will be checked.",
            $processOutput
        );
    }
}
