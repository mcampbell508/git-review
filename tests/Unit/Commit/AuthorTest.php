<?php

namespace Shopworks\Tests\Unit\Commit;

use Shopworks\Git\Review\Commit\Author;
use Shopworks\Tests\UnitTestCase;

class AuthorTest extends UnitTestCase
{
    /** @var  Author $author */
    private $author;

    protected function setUp(): void
    {
        parent::setUp();

        $this->author = new Author(
            "Jon McClane",
            "Mon Jul 30 17:16:26 2018 +0000",
            "jon.mcclane@diehard.com",
            "Jon McClane",
            "Mon Jul 31 12:56:12 2018 +0000",
            "jon.mcclane@diehard.com"
        );
    }

    /** @test */
    public function it_can_present_the_author_name_and_email(): void
    {
        $this->assertEquals("Jon McClane - jon.mcclane@diehard.com", $this->author->presentAuthorNameAndEmail());
    }
}
