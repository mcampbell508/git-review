<?php

namespace Shopworks\Tests\Unit\Commands\TaskSequence;

use Illuminate\Container\Container;
use Mockery;
use Mockery\MockInterface;
use Shopworks\Git\Review\Commands\TaskSequence\Command;
use Shopworks\Git\Review\Process\Process as GitReviewProcess;
use Shopworks\Git\Review\Process\Processor;
use Shopworks\Tests\UnitTestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class CommandTest extends UnitTestCase
{
    /** @var CommandTester $commandTester */
    private $commandTester;
    /** @var Command $command */
    private $command;
    /** @var MockInterface|GitReviewProcess $process */
    private $process;
    /** @var MockInterface|Processor $processor */
    private $processor;

    protected function setUp(): void
    {
        parent::setUp();

        $this->process = Mockery::mock(GitReviewProcess::class);
        $this->processor = Mockery::mock(Processor::class);

        $this->command = new Command($this->process, $this->processor);
        $this->commandTester = $this->getCommandTester();
    }

    /** @test */
    public function it_shows_appropriate_errors_when_no_task_arguments_are_provided(): void
    {
        $this->expectException(\Symfony\Component\Console\Exception\RuntimeException::class);
        $this->commandTester->execute([
            'command' => $this->command->getName(),
        ]);

        $output = $this->commandTester->getDisplay();
        $this->assertEquals(1, $this->commandTester->getStatusCode());
        $this->assertContains("Not enough arguments (missing: \"tasks\").", $output);
    }

    /** @test */
    public function it_can_execute_multiple_tasks_and_provide_feedback_results_of_passing_tasks(): void
    {
        $this->process->shouldReceive('simple')->with("ls -la")
            ->andReturn($process = Mockery::mock(GitReviewProcess::class));
        $this->processor->shouldReceive('process')->with($process, true)->andReturn($process);
        $process->shouldReceive('isSuccessful')->once()->andReturn(true);
        $process->shouldReceive('getExitCode')->once()->andReturn(0);

        $this->process->shouldReceive('simple')->with("date")
            ->andReturn($process = Mockery::mock(GitReviewProcess::class));
        $process->shouldReceive('isSuccessful')->once()->andReturn(true);
        $this->processor->shouldReceive('process')->with($process, true)->andReturn($process);

        $process->shouldReceive('getExitCode')->once()->andReturn(0);
        $this->commandTester->execute([
            'command' => $this->command->getName(),
            'tasks' => [
                'ls -la',
                'date',
            ],
        ]);

        $output = $this->commandTester->getDisplay();
        $this->assertEquals(
            \file_get_contents(
                __DIR__ . '/../../../fixtures/TaskSequence/passing.txt'
            ),
            $output
        );
    }

        /** @test */
    public function it_can_execute_multiple_tasks_and_provide_feedback_results_of_failing_tasks(): void
    {
        $this->process->shouldReceive('simple')->with("ls -la")
            ->andReturn($process = Mockery::mock(GitReviewProcess::class));
        $this->processor->shouldReceive('process')->with($process, true)->andReturn($process);
        $process->shouldReceive('isSuccessful')->once()->andReturn(true);
        $process->shouldReceive('getExitCode')->once()->andReturn(1);

        $this->process->shouldReceive('simple')->with("date")
            ->andReturn($process = Mockery::mock(GitReviewProcess::class));
        $process->shouldReceive('isSuccessful')->once()->andReturn(true);
        $this->processor->shouldReceive('process')->with($process, true)->andReturn($process);

        $process->shouldReceive('getExitCode')->once()->andReturn(0);
        $this->commandTester->execute([
            'command' => $this->command->getName(),
            'tasks' => [
                'ls -la',
                'date',
            ],
        ]);

        $output = $this->commandTester->getDisplay();
        $this->assertEquals(
            \file_get_contents(
                __DIR__ . '/../../../fixtures/TaskSequence/failing.txt'
            ),
            $output
        );
    }

    /** @test */
    public function it_can_execute_multiple_tasks_and_hide_results_feedback_when_no_results_option_is_provided(): void
    {
        $this->process->shouldReceive('simple')->with("ls -la")
            ->andReturn($process = Mockery::mock(GitReviewProcess::class));
        $this->processor->shouldReceive('process')->with($process, true)->andReturn($process);
        $process->shouldReceive('isSuccessful')->once()->andReturn(true);
        $process->shouldReceive('getExitCode')->once()->andReturn(0);

        $this->process->shouldReceive('simple')->with("date")
            ->andReturn($process = Mockery::mock(GitReviewProcess::class));
        $process->shouldReceive('isSuccessful')->once()->andReturn(true);
        $this->processor->shouldReceive('process')->with($process, true)->andReturn($process);

        $process->shouldReceive('getExitCode')->once()->andReturn(0);
        $this->commandTester->execute([
            'command' => $this->command->getName(),
            'tasks' => [
                'ls -la',
                'date',
            ],
            '--noResults' => true,
        ]);

        $output = $this->commandTester->getDisplay();
        $this->assertEquals(
            \file_get_contents(
                __DIR__ . '/../../../fixtures/TaskSequence/passing-with-no-results.txt'
            ),
            $output
        );
    }

    private function getCommandTester(): CommandTester
    {
        $consoleApplication = new Application();

        $this->command->setLaravel(new Container());

        $consoleApplication->add($this->command);
        $command = $consoleApplication->find('task-sequence');

        return new CommandTester($command);
    }
}
