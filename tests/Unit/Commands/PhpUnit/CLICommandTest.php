<?php

namespace Shopworks\Tests\Unit\Commands\PhpUnit;

use Illuminate\Support\Collection;
use Shopworks\Git\Review\Commands\PhpUnit\CLICommand;
use Shopworks\Tests\UnitTestCase;

class CLICommandTest extends UnitTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    /** @test */
    public function it_can_build_up_a_collection_of_phpunit_commands_to_execute(): void
    {
        $command = new CLICommand(['tests/', 'tests/ExampleTest.php']);

        $this->assertEquals(
            Collection::make([
                "php vendor/bin/phpunit tests/",
                "php vendor/bin/phpunit tests/ExampleTest.php",
            ]),
            $command->getCommands()
        );
    }
}
