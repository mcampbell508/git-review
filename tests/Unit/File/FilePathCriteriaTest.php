<?php

namespace Shopworks\Tests\Unit\File;

use PHPUnit\Framework\Assert;
use Shopworks\Git\Review\File\FilePathCriteria;
use Shopworks\Tests\UnitTestCase;

class FilePathCriteriaTest extends UnitTestCase
{
    /** @var FilePathCriteria $fileCriteria */
    private $fileCriteria;

    protected function setUp(): void
    {
        parent::setUp();

        $this->fileCriteria = new FilePathCriteria(['A', 'B', 'C'], ['X', 'Y', 'Z']);
    }

    /**
     * @test
     */
    public function it_can_retrieve_an_array_of_paths_to_be_included(): void
    {
        Assert::assertEquals(['A', 'B', 'C'], $this->fileCriteria->getIncludePaths());
    }

    /**
     * @test
     */
    public function it_can_retrieve_an_array_of_paths_to_be_excluded(): void
    {
        Assert::assertEquals(['X', 'Y', 'Z'], $this->fileCriteria->getExcludePaths());
    }
}
