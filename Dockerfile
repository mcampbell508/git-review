FROM php:7.1.26-cli-alpine3.9

LABEL maintainer="Matt Campbell <matt.campbell@theshopworks.com>"
LABEL authors="Matt Campbell <matt.campbell@theshopworks.com>, Pedro Coutinho <pedro.coutinho@theshopworks.com>"

ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_HOME /composer
ENV PATH /composer/vendor/bin:$PATH

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

RUN apk --update --no-cache add bash git yarn \
    && rm -rf /var/cache/apk/* /var/tmp/* /tmp/*

COPY ./ /composer/

RUN echo "memory_limit=1024M" > $PHP_INI_DIR/conf.d/memory-limit.ini

RUN cd /composer && php /usr/bin/composer install --no-dev --no-interaction --no-progress \
    && php /usr/bin/composer clearcache && ln -s /composer/git-review /composer/vendor/bin

COPY docker/package.json /nodejs/package.json
COPY docker/yarn.lock /nodejs/yarn.lock

RUN cd /nodejs && yarn install

VOLUME ["/app"]
WORKDIR /app

ENTRYPOINT ["git-review"]
