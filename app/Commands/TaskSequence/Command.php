<?php

namespace Shopworks\Git\Review\Commands\TaskSequence;

use Illuminate\Console\Command as BaseCommand;
use Illuminate\Support\Collection;
use Shopworks\Git\Review\Process\Process;
use Shopworks\Git\Review\Process\Processor;
use Shopworks\Git\Review\Process\ProcessResult;
use Shopworks\Git\Review\Process\ProcessResultCollection;

class Command extends BaseCommand
{
    protected $signature = 'task-sequence {tasks*} {--noResults}';
    protected $description = 'Process several tasks in a sequential fashion and then receive result of tasks '
        . 'in a single place.';

    private $processor;
    private $process;

    public function __construct(Process $process, Processor $processor)
    {
        parent::__construct();

        $this->process = $process;
        $this->processor = $processor;
    }

    public function handle(): int
    {
        $tasks = Collection::make($this->argument('tasks'));
        $results = $this->runCommands($tasks);

        if (!$this->option('noResults')) {
            $this->compileResults($results);
        }

        if ($results->hasErrors()) {
            $this->getOutput()->error(
                "{$results->failCount()} out of {$results->count()} tasks have failed."
            );

            return Process::EXIT_CODE_FAILED;
        }

        $this->getOutput()->success("{$results->count()} out of {$results->count()} tasks have passed. Good job!");

        return Process::EXIT_CODE_SUCCESS;
    }

    private function runCommands(Collection $commands): ProcessResultCollection
    {
        return $commands->reduce(function (ProcessResultCollection $results, string $command) {
            $this->getOutput()->title("Running command: \"{$command}\"");
            $process = $this->runCommand($command);
            $this->getOutput()->newLine();

            $results->push(new ProcessResult($command, $process->getExitCode(), $process->isSuccessful()));

            return $results;
        }, ProcessResultCollection::make());
    }

    private function runCommand(string $command): Process
    {
        $process = $this->process->simple($command);

        return $this->processor->process($process, true);
    }

    private function compileResults(ProcessResultCollection $results): void
    {
        $this->getOutput()->title("Results:");
        $this->getOutput()->listing($results->map(function (ProcessResult $result) {
            return $result->getResult();
        })->toArray());
    }
}
