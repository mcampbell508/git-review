<?php

declare(strict_types=1);

namespace Shopworks\Git\Review\Commands\PhpCsFixer;

use Shopworks\Git\Review\BaseCommand;
use Shopworks\Git\Review\Commands\CliCommandContract;

class Command extends BaseCommand
{
    protected $signature = 'php-cs-fixer';
    protected $toolName = 'PHP-CS-Fixer';
    protected $config = 'tools.php_cs_fixer';
    protected $configPaths = 'tools.php_cs_fixer.paths';

    protected function resolveCommand(array $ymlConfig, array $paths): CliCommandContract
    {
        return new CLICommand($ymlConfig, $paths);
    }
}
