<?php

declare(strict_types=1);

namespace Shopworks\Git\Review\Commands\PhpUnit;

use Illuminate\Support\Collection;

class CLICommand
{
    private $filePaths;

    public function __construct(array $files)
    {
        $this->filePaths = $files;
    }

    public function getCommands(): Collection
    {
        if (empty($this->filePaths)) {
            return Collection::make(['php vendor/bin/phpunit']);
        }

        return Collection::make($this->filePaths)->map(function (string $file) {
            return "php vendor/bin/phpunit {$file}";
        });
    }
}
