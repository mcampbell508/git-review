<?php

declare(strict_types=1);

namespace Shopworks\Git\Review\Providers;

use Illuminate\Support\ServiceProvider;
use OndraM\CiDetector\CiDetector;
use Shopworks\Git\Review\Commands\ESLint\Command as ESLintCommand;
use Shopworks\Git\Review\Commands\PhpCodeBeautifier\Command as PhpCodeBeautifier;
use Shopworks\Git\Review\Commands\PhpCodeSniffer\Command as PhpCodeSniffer;
use Shopworks\Git\Review\Commands\PhpCsFixer\Command as PhpCsFixerCommand;
use Shopworks\Git\Review\Commands\PhpMessDetector\Command as PhpMessDetector;
use Shopworks\Git\Review\Commands\PhpParallelLint\Command as PhpParallelLintCommand;
use Shopworks\Git\Review\Commands\PhpStan\Command as PhpStanCommand;
use Shopworks\Git\Review\Commands\PhpUnit\Command as PhpUnitCommand;
use Shopworks\Git\Review\Commands\TaskSequence\Command as TaskSequenceCommand;
use Shopworks\Git\Review\Commit\CommitLogParser;
use Shopworks\Git\Review\Commit\CommitParser;
use Shopworks\Git\Review\Process\Process;
use Shopworks\Git\Review\Process\Processor;
use Shopworks\Git\Review\Utility\CoreFunctions;
use Shopworks\Git\Review\VersionControl\GitBranch;
use Shopworks\Git\Review\Yml\YmlConfiguration;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
    }

    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(YmlConfiguration::class, function () {
            return new YmlConfiguration(\getcwd());
        });

        $this->app->bind(GitBranch::class, function () {
            return new GitBranch(
                new CiDetector(),
                \getcwd(),
                new Processor(),
                new CommitLogParser(new CoreFunctions()),
                new CommitParser(new Processor(), new Process(), \getcwd())
            );
        });

        $this->commands([
            ESLintCommand::class,
            PhpCsFixerCommand::class,
            PhpCodeSniffer::class,
            PhpCodeBeautifier::class,
            PhpMessDetector::class,
            PhpParallelLintCommand::class,
            PhpStanCommand::class,
            TaskSequenceCommand::class,
            PhpUnitCommand::class,
        ]);
    }
}
