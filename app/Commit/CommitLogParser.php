<?php

namespace Shopworks\Git\Review\Commit;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Shopworks\Git\Review\Utility\CoreFunctions;

class CommitLogParser
{
    private $coreFunctions;

    public function __construct(CoreFunctions $coreFunctions)
    {
        $this->coreFunctions = $coreFunctions;
    }

    public function parse(string $commitLog): Collection
    {
        $identifier = $this->ensureCommitsAreSeparatedByUniqueIdentifier($commitLog);

        $commits = Collection::make(\explode(\PHP_EOL, $commitLog))
            ->reduce(function (Collection $commitsCollection, $string) use ($identifier): Collection {
                if (Str::startsWith($string, "Hash: ")) {
                    $commitsCollection->push($identifier);
                }

                if (\mb_strlen($string) === 0) {
                    return $commitsCollection;
                }

                $commitsCollection->push(\trim($string));

                return $commitsCollection;
            }, Collection::make());

        $commits = Collection::make(\explode($identifier, \implode(\PHP_EOL, $commits->toArray())));
        $commits->shift();

        return $commits;
    }

    private function ensureCommitsAreSeparatedByUniqueIdentifier(string $commits): string
    {
        $identifier = "___GIT_REVIEW___" . $this->coreFunctions->generateRandomNumber();

        while (Str::contains($commits, $identifier)) {
            $identifier .= $this->coreFunctions->generateRandomNumber();
        }

        return $identifier;
    }
}
